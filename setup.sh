sudo apt-get update
sudo apt-get install -y hostapd
cp hostapd.conf /etc/hostapd/

# Installing drivers for realtek chip set
sudo apt-get install bc raspberrypi-kernel-headers -y
sudo apt-get install dkms -y
cd /opt
sudo git clone https://gitlab.com/kalilinux/packages/realtek-rtl88xxau-dkms.git && \
(cd realtek-rtl88xxau-dkms/
model=$(cat /proc/device-tree/model | awk '{print $3 $5'})

if [ $model = "3B+" ] || [ $model = "4B+" ]; then
    # Commands for rpi 3B+ & 4B+
    sed -i 's/CONFIG_PLATFORM_I386_PC = y/CONFIG_PLATFORM_I386_PC = n/g' Makefile
    sed -i 's/CONFIG_PLATFORM_ARM64_RPI = n/CONFIG_PLATFORM_ARM64_RPI = y/g' Makefile
else
    # Commands for rpi 0/1/2/3/
    sudo sed -i 's/CONFIG_PLATFORM_I386_PC = y/CONFIG_PLATFORM_I386_PC = n/g' Makefile
    sudo sed -i 's/CONFIG_PLATFORM_ARM_RPI = n/CONFIG_PLATFORM_ARM_RPI = y/g' Makefile
fi
sudo make && sudo make install) || echo "Driver already installed"

# Enabling it on boot
sudo echo DAEMON_CONF="/etc/hostapd/hostapd.conf" >> /etc/default/hostapd
sudo update-rc.d hostapd enable

echo "Rebooting in 10 seconds, insert your network on reboot"
sleep 10;
