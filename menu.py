# pip install pyfiglet
import pyfiglet
from pyfiglet import Figlet
custom_fig = Figlet(font='skateord')
print(custom_fig.renderText('tunneling'))

ascii_banner = pyfiglet.figlet_format("network-tools")
print(ascii_banner)
